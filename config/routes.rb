Rails.application.routes.draw do
  
  devise_for :users
  
  resources :recipes do
    member do
      get :delete
    end
  end

  resources :categories do
    member do
      get :delete
    end
  end

  root "recipes#index"
  
end
