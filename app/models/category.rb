class Category < ActiveRecord::Base

  # Associations
  has_many :recipes

  # Validation rules
  validates :name, presence: true, length: { maximum: 255 }

end
