class Recipe < ActiveRecord::Base

  # Associations
  belongs_to :user
  belongs_to :category
  has_many :ingredients
  has_many :directions

  accepts_nested_attributes_for :ingredients, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :directions,  reject_if: :all_blank, allow_destroy: true

  # Validation rules
  validates :title,       presence: true, length: { maximum: 255 }
  validates :description, presence: true, length: { minimum: 10  }

  # Paperclip configuration - File uploading
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
  validates_attachment  :image, content_type: { content_type: /\Aimage\/.*\z/ }, size: { in: 0..20.kilobytes }

end
