class RecipePolicy

  attr_reader :user, :recipe
  
  def initialize(user, recipe)
    @user   = user
    @recipe = recipe
  end

  def edit?
    @user == @recipe.user
  end

  def delete?
    @user == @recipe.user || @user.role == "admin"
  end
end
