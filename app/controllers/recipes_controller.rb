class RecipesController < ApplicationController
  
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_recipe, only: [:show, :edit, :update, :delete, :destroy]
  before_action :list_categories, only: [:index, :show]

  def index
    @recipes = Recipe.all.includes(:category)
    @recipes = @recipes.includes(:user) if user_signed_in?

    unless params[:category].blank?
      category_id = Category.find(params[:category]).id      
      @recipes = @recipes.where(category_id: category_id)
    end
  end

  def show
  end

  def new
    @recipe = current_user.recipes.build
  end

  def edit
    authorize @recipe
  end

  def create
    @recipe = current_user.recipes.build(recipe_params)

    respond_to do |format|
      if @recipe.save
        format.html { redirect_to @recipe, notice: 'Recipe was successfully created.' }
        format.json { render :show, status: :created, location: @recipe }
      else
        format.html { render :new }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @recipe.update(recipe_params)
        format.html { redirect_to @recipe, notice: 'Recipe was successfully updated.' }
        format.json { render :show, status: :ok, location: @recipe }
      else
        format.html { render :edit }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete
    authorize @recipe
  end

  def destroy
    
    @recipe.image = nil
    @recipe.destroy
    
    respond_to do |format|
      format.html { redirect_to recipes_url, notice: 'Recipe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    def recipe_params
      params.require(:recipe).permit(:title, :description, :category_id, :image, ingredients_attributes: [:id, :name, :_destroy], directions_attributes: [:id, :step, :_destroy])
    end

    def list_categories
      @categories = Category.all().order("name")
    end
end
